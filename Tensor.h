#pragma once
#include <cmath>


namespace constants
{
    template<size_t... D> constexpr size_t size = (1 * ... * D);

    //The ternary within the template parameter is only necessary to stop endless recursion in the template evaluation.
    //The actual statement, however, is never called and evaluated when N is already 0
    template<size_t N, size_t D0=0, size_t... D> constexpr size_t stride = N ? stride<N?N-1:N,D...> : (1 * ... * D);
}


template<typename T, size_t... D>
struct Tensor
{
    T data[constants::size<D...>];  //public data for aggregate initilization

    using type = T;

    static constexpr size_t size();             //total number of elements
    static constexpr size_t ndim();             //number of dimensions
    static constexpr size_t dim(size_t N);      //size of the given axis
    static constexpr size_t stride(size_t N);   //stride of the given axis in elements

    constexpr T* ptr();                                                         //raw pointer access
    constexpr T& operator[](size_t i);                                          //1D accessor
    constexpr const T& operator[](size_t i) const;                              //const version of 1D accessor
    constexpr T& operator[](const Tensor<size_t,sizeof...(D)>& i);              //N-dimensionsal Accessor
    constexpr const T& operator[](const Tensor<size_t,sizeof...(D)>& i) const;  //same but const

    constexpr T sum() const;        //sum of all elements
    constexpr T product() const;    //product of all elements
    constexpr T min() const;        //min of all elements
    constexpr T max() const;        //max of all elements
    constexpr T normsq() const;     //sum of square of all elements
    constexpr T norm() const;       //square root of normsq

    constexpr void zero();          //zero all elements


    constexpr Tensor<T,D...> operator+() const;
    constexpr Tensor<T,D...> operator-() const;

    template<typename T2> constexpr Tensor<T,D...>& operator+=(const Tensor<T2,D...>& in);    //adds tensor of same dimensions inplace
    template<typename T2> constexpr Tensor<T,D...>& operator+=(const T2& in);                 //adds constant inplace
    template<typename T2> constexpr Tensor<T,D...>& operator-=(const Tensor<T2,D...>& in);    //subtracts tensor of same dimensions inplace
    template<typename T2> constexpr Tensor<T,D...>& operator-=(const T2& in);                 //subtracts constant inplace
    template<typename T2> constexpr Tensor<T,D...>& operator*=(const Tensor<T2,D...>& in);    //multiplies by tensor of same dimensions inplace
    template<typename T2> constexpr Tensor<T,D...>& operator*=(const T2& in);                 //multiplies by constant inplace
    template<typename T2> constexpr Tensor<T,D...>& operator/=(const Tensor<T2,D...>& in);    //divides by tensor of same dimensions inplace
    template<typename T2> constexpr Tensor<T,D...>& operator/=(const T2& in);                 //divides by constant inplace


    template<size_t... DN> using TypeAddDimsFront = Tensor<T, DN... , D...>;    //type of the Tensor if one were to add dimensions to the front
    template<size_t... DN> using TypeAddDimsBack = Tensor<T, D... , DN...>;     //type of the Tensor if one were to add dimensions to the back
};



//helper function to calculate strides by at compile time stripping first N elements and multiplying the rest; non-member; recursive but depth only = size...(D)
template<size_t N, size_t... D>
constexpr size_t index(const Tensor<size_t,sizeof...(D)>& indices)
{
    if constexpr(N+1 < sizeof...(D))
    {
        return indices[N]*constants::stride<N,D...> + index<N+1,D...>(indices);
    }
    else
    {
        return indices[N]*constants::stride<N,D...>;
    }
}




template<typename T, size_t... D>
constexpr size_t Tensor<T,D...>::size()
{
    return (1 * ... * D);
}

template<typename T, size_t... D>
constexpr size_t Tensor<T,D...>::ndim()
{
    return sizeof...(D);
}

//slow at runtime
template<typename T, size_t... D>
constexpr size_t Tensor<T,D...>::stride(size_t N)
{
    size_t out = 1;
    for(size_t i=sizeof...(D)-1; i > N; i--)
    {
        out *= dim(i);
    }
    return out;
}

template<typename T, size_t... D>
constexpr size_t Tensor<T,D...>::dim(size_t N)
{
    size_t dims[sizeof...(D)] = {D...};
    return dims[N];
}

template<typename T, size_t... D>
constexpr T* Tensor<T,D...>::ptr()
{
    return data;
}

template<typename T, size_t... D>
constexpr T& Tensor<T,D...>::operator[](size_t i)
{
    return data[i];
}

template<typename T, size_t... D>
constexpr const T& Tensor<T,D...>::operator[](size_t i) const
{
    return data[i];
}

template<typename T, size_t... D>
constexpr T& Tensor<T,D...>::operator[](const Tensor<size_t,sizeof...(D)>& indices)
{
    return data[index<0,D...>(indices)];
}

template<typename T, size_t... D>
constexpr const T& Tensor<T,D...>::operator[](const Tensor<size_t,sizeof...(D)>& indices) const
{
    return data[index<0,D...>(indices)];
}

template<typename T, size_t... D>
constexpr void Tensor<T,D...>::zero()
{
    T val(0);
    for(size_t i=0; i<constants::size<D...>; i++)
    {
        data[i] = val;
    }
}

template<typename T, size_t... D>
constexpr T Tensor<T,D...>::sum() const
{
    T out = data[0];
    for(size_t i=1; i<constants::size<D...>; i++)
    {
        out += data[i];
    }
    return out;
}

template<typename T, size_t... D>
constexpr T Tensor<T,D...>::product() const
{
    T out = data[0];
    for(size_t i=1; i<constants::size<D...>; i++)
    {
        out *= data[i];
    }
    return out;
}

template<typename T, size_t... D>
constexpr T Tensor<T,D...>::min() const
{
    T out = data[0];
    for(size_t i=1; i<constants::size<D...>; i++)
    {
        if(data[i] < out)
        {
            out = data[i];
        }
    }
    return out;
}

template<typename T, size_t... D>
constexpr T Tensor<T,D...>::max() const
{
    T out = data[0];
    for(size_t i=1; i<constants::size<D...>; i++)
    {
        if(data[i] > out)
        {
            out = data[i];
        }
    }
    return out;
}

template<typename T, size_t... D>
constexpr T Tensor<T,D...>::normsq() const
{
    T out = data[0] * data[0];
    for(size_t i=1; i<constants::size<D...>; i++)
    {
        out += data[i] * data[i];
    }
    return out;
}

template<typename T, size_t... D>
constexpr T Tensor<T,D...>::norm() const
{
    return std::sqrt(normsq());
}


template<typename T, size_t... D>
constexpr Tensor<T,D...> Tensor<T,D...>::operator+() const
{
    Tensor<T,D...> out{};
    for(size_t i=0; i<constants::size<D...>; i++)
    {
        out[i] = +data[i];
    }
    return out;
}

template<typename T, size_t... D>
constexpr Tensor<T,D...> Tensor<T,D...>::operator-() const
{
    Tensor<T,D...> out{};
    for(size_t i=0; i<constants::size<D...>; i++)
    {
        out[i] = -data[i];
    }
    return out;
}


template<typename T, size_t... D> template<typename T2> 
constexpr Tensor<T,D...>& Tensor<T,D...>::operator+=(const Tensor<T2,D...>& in)
{
    for(size_t i=0; i<constants::size<D...>; i++)
    {
        data[i] += in[i];
    }
    return *this;
}

template<typename T, size_t... D> template<typename T2> 
constexpr Tensor<T,D...>& Tensor<T,D...>::operator+=(const T2& in)
{
    for(size_t i=0; i<constants::size<D...>; i++)
    {
        data[i] += in;
    }
    return *this;
}

template<typename T, size_t... D> template<typename T2> 
constexpr Tensor<T,D...>& Tensor<T,D...>::operator-=(const Tensor<T2,D...>& in)
{
    for(size_t i=0; i<constants::size<D...>; i++)
    {
        data[i] -= in[i];
    }
    return *this;
}

template<typename T, size_t... D> template<typename T2> 
constexpr Tensor<T,D...>& Tensor<T,D...>::operator-=(const T2& in)
{
    for(size_t i=0; i<constants::size<D...>; i++)
    {
        data[i] -= in;
    }
    return *this;
}

template<typename T, size_t... D> template<typename T2> 
constexpr Tensor<T,D...>& Tensor<T,D...>::operator*=(const Tensor<T2,D...>& in)
{
    for(size_t i=0; i<constants::size<D...>; i++)
    {
        data[i] *= in[i];
    }
    return *this;
}

template<typename T, size_t... D> template<typename T2> 
constexpr Tensor<T,D...>& Tensor<T,D...>::operator*=(const T2& in)
{
    for(size_t i=0; i<constants::size<D...>; i++)
    {
        data[i] *= in;
    }
    return *this;
}

template<typename T, size_t... D> template<typename T2> 
constexpr Tensor<T,D...>& Tensor<T,D...>::operator/=(const Tensor<T2,D...>& in)
{
    for(size_t i=0; i<constants::size<D...>; i++)
    {
        data[i] /= in[i];
    }
    return *this;
}

template<typename T, size_t... D> template<typename T2> 
constexpr Tensor<T,D...>& Tensor<T,D...>::operator/=(const T2& in)
{
    for(size_t i=0; i<constants::size<D...>; i++)
    {
        data[i] /= in;
    }
    return *this;
}



template<typename T, size_t... D>
constexpr Tensor<T,D...> operator+(const Tensor<T,D...>& lhs, const Tensor<T,D...>& rhs)
{
    Tensor<T,D...> out{};
    for(size_t i=0; i<constants::size<D...>; i++)
    {
        out.data[i] = lhs.data[i] + rhs.data[i];
    }
    return out;
}

template<typename T, size_t... D>
constexpr Tensor<T,D...> operator-(const Tensor<T,D...>& lhs, const Tensor<T,D...>& rhs)
{
    Tensor<T,D...> out{};
    for(size_t i=0; i<constants::size<D...>; i++)
    {
        out.data[i] = lhs.data[i] - rhs.data[i];
    }
    return out;
}

template<typename T, size_t... D>
constexpr Tensor<T,D...> operator*(const Tensor<T,D...>& lhs, const Tensor<T,D...>& rhs)
{
    Tensor<T,D...> out{};
    for(size_t i=0; i<constants::size<D...>; i++)
    {
        out.data[i] = lhs.data[i] * rhs.data[i];
    }
    return out;
}

template<typename T, size_t... D>
constexpr Tensor<T,D...> operator/(const Tensor<T,D...>& lhs, const Tensor<T,D...>& rhs)
{
    Tensor<T,D...> out{};
    for(size_t i=0; i<constants::size<D...>; i++)
    {
        out.data[i] = lhs.data[i] / rhs.data[i];
    }
    return out;
}


template<typename T, size_t... D>
constexpr Tensor<T,D...> operator+(const Tensor<T,D...>& lhs, const T& rhs)
{
    Tensor<T,D...> out{};
    for(size_t i=0; i<constants::size<D...>; i++)
    {
        out.data[i] = lhs.data[i] + rhs;
    }
    return out;
}

template<typename T, size_t... D>
constexpr Tensor<T,D...> operator-(const Tensor<T,D...>& lhs, const T& rhs)
{
    Tensor<T,D...> out{};
    for(size_t i=0; i<constants::size<D...>; i++)
    {
        out.data[i] = lhs.data[i] - rhs;
    }
    return out;
}

template<typename T, size_t... D>
constexpr Tensor<T,D...> operator*(const Tensor<T,D...>& lhs, const T& rhs)
{
    Tensor<T,D...> out{};
    for(size_t i=0; i<constants::size<D...>; i++)
    {
        out.data[i] = lhs.data[i] * rhs;
    }
    return out;
}

template<typename T, size_t... D>
constexpr Tensor<T,D...> operator/(const Tensor<T,D...>& lhs, const T& rhs)
{
    Tensor<T,D...> out{};
    for(size_t i=0; i<constants::size<D...>; i++)
    {
        out.data[i] = lhs.data[i] / rhs;
    }
    return out;
}


template<typename T, size_t... D>
constexpr Tensor<T,D...> operator+(const T& lhs, const Tensor<T,D...>& rhs)
{
    Tensor<T,D...> out{};
    for(size_t i=0; i<constants::size<D...>; i++)
    {
        out.data[i] = lhs + rhs.data[i];
    }
    return out;
}

template<typename T, size_t... D>
constexpr Tensor<T,D...> operator-(const T& lhs, const Tensor<T,D...>& rhs)
{
    Tensor<T,D...> out{};
    for(size_t i=0; i<constants::size<D...>; i++)
    {
        out.data[i] = lhs - rhs.data[i];
    }
    return out;
}

template<typename T, size_t... D>
constexpr Tensor<T,D...> operator*(const T& lhs, const Tensor<T,D...>& rhs)
{
    Tensor<T,D...> out{};
    for(size_t i=0; i<constants::size<D...>; i++)
    {
        out.data[i] = lhs * rhs.data[i];
    }
    return out;
}

template<typename T, size_t... D>
constexpr Tensor<T,D...> operator/(const T& lhs, const Tensor<T,D...>& rhs)
{
    Tensor<T,D...> out{};
    for(size_t i=0; i<constants::size<D...>; i++)
    {
        out.data[i] = lhs / rhs.data[i];
    }
    return out;
}

template<typename T, size_t... D>
constexpr bool operator==(const Tensor<T,D...>& lhs, const Tensor<T,D...>& rhs)
{
    for(size_t i=0; i<constants::size<D...>; i++)
    {
        if(lhs.data[i] != rhs.data[i])
        {
            return false;
        }
    }
    return true;
}

template<typename T, size_t... D>
constexpr bool operator!=(const Tensor<T,D...>& lhs, const Tensor<T,D...>& rhs)
{
    for(size_t i=0; i<constants::size<D...>; i++)
    {
        if(lhs.data[i] != rhs.data[i])
        {
            return true;
        }
    }
    return false;
}

template<typename T, size_t... D> 
constexpr void MAC(Tensor<T,D...>& out, const Tensor<T,D...>& lhs, const Tensor<T,D...>& rhs)
{
    for(size_t i=0; i<constants::size<D...>; i++)
    {
        out.data[i] += lhs.data[i]*rhs.data[i];
    }
}

template<typename T, size_t... D> 
constexpr void MAC(Tensor<T,D...>& out, const Tensor<T,D...>& lhs, const T& rhs)
{
    for(size_t i=0; i<constants::size<D...>; i++)
    {
        out.data[i] += lhs.data[i]*rhs;
    }
}

template<typename T, size_t... D> 
constexpr void MAC(Tensor<T,D...>& out, const T& lhs, const Tensor<T,D...>& rhs)
{
    for(size_t i=0; i<constants::size<D...>; i++)
    {
        out.data[i] += lhs*rhs.data[i];
    }
}

template<typename T, size_t... D0, size_t... D1> 
constexpr void OPAC(Tensor<T,D0...,D1...>& out, const Tensor<T,D0...>& lhs, const Tensor<T,D1...>& rhs)
{
    for(size_t i=0; i<constants::size<D0...>; i++)
    {
        size_t offset = i*constants::size<D1...>;
        for(size_t j=0; j<constants::size<D1...>; j++)
        {
            out.data[offset] += lhs.data[i]*rhs.data[j];
            offset++;
        }
    }
}

template<typename T, size_t... D>
constexpr T dot(const Tensor<T,D...>& lhs, const Tensor<T,D...>& rhs)
{
    T out = lhs.data[0]*rhs.data[0];
    for(size_t i=1; i<constants::size<D...>; i++)
    {
        out += lhs.data[i]*rhs.data[i];
    }
    return out;
}

template<typename T>
constexpr Tensor<T,3> cross(const Tensor<T,3>& lhs, const Tensor<T,3>& rhs)
{
    return Tensor<T,3>{ lhs.data[1]*rhs.data[2] - lhs.data[2]*rhs.data[1], 
                        lhs.data[2]*rhs.data[0] - lhs.data[0]*rhs.data[2], 
                        lhs.data[0]*rhs.data[1] - lhs.data[1]*rhs.data[0] };
}

namespace internal
{
//compute difference of products (dop) a*b - c*d in a way that avoids catastrophic cancellation.
//see https://pharr.org/matt/blog/2019/11/03/difference-of-floats.
template<typename T>
constexpr T dop(const T& a, const T& b, const T& c, const T& d)
{
    T cd = c * d;
    T err = std::fma(-c, d, cd);
    T dop = std::fma(a, b, -cd);
    return dop + err;
}
}

// Compute cross product using the above dop function to avoid catastrophic cancellation.
// This is quite a bit slower than the naive implementation of the cross product, but probably more precise.
template<typename T>
constexpr Tensor<T,3> cross2(const Tensor<T,3>& lhs, const Tensor<T,3>& rhs)
{
    return Tensor<T,3>{ internal::dop(lhs.data[1], rhs.data[2], lhs.data[2], rhs.data[1]), 
                        internal::dop(lhs.data[2], rhs.data[0], lhs.data[0], rhs.data[2]), 
                        internal::dop(lhs.data[0], rhs.data[1], lhs.data[1], rhs.data[0])};
}

template<typename T, size_t D1, size_t D2, size_t D3>
constexpr Tensor<T,D1,D3> mmp(const Tensor<T,D1,D2>& lhs, const Tensor<T,D2,D3>& rhs)
{
    Tensor<T,D1,D3> out{};
    for(size_t i=0; i<D1; i++)
    {
        for(size_t j=0; j<D3; j++)
        {
            T& val = out.data[i*D3 + j] = 0.0;
            for(size_t k=0; k<D2; k++)
            {
                val += lhs[i*D2 + k] * rhs[k*D3 + j];
            }
        }
    }
    return out;
}

template<typename T, size_t D1, size_t D2>
constexpr Tensor<T,D1> mvp(const Tensor<T,D1,D2>& lhs, const Tensor<T,D2>& rhs)
{
    Tensor<T,D1> out{};
    for(size_t i=0; i<D1; i++)
    {
        T& val = out[i] = 0.0;
        for(size_t j=0; j<D2; j++)
        {
            val += lhs[i*D2 + j] * rhs[j];
        }
    }
    return out;
}

template<typename T, size_t D1, size_t D2>
constexpr Tensor<T,D2> vmp(const Tensor<T,D1>& lhs, const Tensor<T,D1,D2>& rhs)
{
    Tensor<T,D2> out{};
    for(size_t i=0; i<D2; i++)
    {
        T& val = out[i] = 0.0;
        for(size_t j=0; j<D1; j++)
        {
            val += lhs[j] * rhs[j*D2 + i];
        }
    }
    return out;
}




template<size_t N=0, typename T, size_t... D, typename STREAMTYPE, typename... I> 
inline STREAMTYPE& printToStream(const Tensor<T,D...>& tensor, STREAMTYPE& stream, I... i)
{
    if constexpr(sizeof...(D) > 0)
    {
        size_t dims[sizeof...(D)] = {D...};
        stream<<"[";
        for(size_t j=0; j<dims[N]; j++)
        {
            if constexpr(N+1 >= sizeof...(D))
            {
                stream<<tensor[{i..., j}];
            }
            else
            {
                printToStream<N+1>(tensor, stream, i..., j);
            }

            if(j < dims[N]-1)
            {
                stream<<",";
            }
        }
        stream<<"]";
    }
    else
    {
        stream<<tensor[0];
    }
    return stream;
}

template<typename STREAMTYPE, typename T, size_t... D>
inline STREAMTYPE& operator<<(STREAMTYPE& stream, const Tensor<T,D...>& in)
{
    return printToStream(in, stream);
}







//////////////////////////////////////////////////////////////////////////////////////////////////////////////
//everything below here works, but is still somewhat experimental
//////////////////////////////////////////////////////////////////////////////////////////////////////////////

template<size_t D, size_t AXIS=D-1>
constexpr bool iterate(const Tensor<size_t,D>& min, const Tensor<size_t,D>& max, Tensor<size_t,D>& current)
{
    current[AXIS]++;
    if(current[AXIS] <= max[AXIS])
    {
        return true;
    }

    if constexpr(!AXIS)
    {
        return false;
    }
    else
    {
        current[AXIS] = min[AXIS];
        return iterate<D,AXIS-1>(min, max, current);
    }
}



template<typename T, size_t... D0, size_t... D1>
constexpr Tensor<T,D0...>::template TypeAddDimsBack<D1...> outer_product(const Tensor<T,D0...>& lhs, const Tensor<T,D1...>& rhs)
{
    if constexpr(sizeof...(D0) > 0 && sizeof...(D1) > 0)
    {
        typename Tensor<T,D0...>::template TypeAddDimsBack<D1...> out{};
        for(size_t i=0; i<constants::size<D0...>; i++)
        {
            size_t offset = i*constants::size<D1...>;
            for(size_t j=0; j<constants::size<D1...>; j++)
            {
                out.data[offset] = lhs.data[i]*rhs.data[j];
                offset++;
            }
        }
        return out;
    }
    else if constexpr(sizeof...(D0) > 0)
    {
        return lhs * rhs.data[0];
    }
    else if constexpr(sizeof...(D1) > 0)
    {
        return lhs.data[0] * rhs;
    }
    else
    {
        return lhs.data[0] * rhs.data[0];
    }
}
