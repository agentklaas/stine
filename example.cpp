#include "Tensor.h"
#include <iostream>

int main()
{
    Tensor<double,3> x{1.0, 0.0, 0.0};
    Tensor<double,3> y{0.0, 1.0, 0.0};
    Tensor<double,3> z{0.0, 0.0, 1.0};

    std::cout<<"x = "<<x<<"\n";
    std::cout<<"y = "<<y<<"\n";
    std::cout<<"z = "<<z<<"\n\n";

    std::cout<<"2.0*(x+y+z) = "<<2.0*(x+y+z)<<"\n";
    std::cout<<"(x+y+z)/2.0 = "<<(x+y+z)/2.0<<"\n";
    std::cout<<"dot(x,y) = "<<dot(x,y)<<"\n";
    std::cout<<"x.normsq() = "<<x.normsq()<<"\n";
    std::cout<<"outer_product(x,y) = "<<outer_product(x,y)<<"\n\n";

    
    Tensor<double,3,3> t{0,1,2, 3,4,5, 6,7,8};
    std::cout<<"t = "<<t<<"\n\n";

    for(size_t i=0; i<9; i++)
    {
        std::cout<<"t["<<i<<"] = "<<t[i]<<"\n";
    }
    std::cout<<"\n";

    for(size_t i=0; i<3; i++)
    {
        for(size_t j=0; j<3; j++)
        {
            std::cout<<"t[{"<<i<<","<<j<<"}] = "<<t[{i,j}]<<"\n";
        }
    }
    std::cout<<"\n";


    Tensor<double,4> v{0,1,2,3};
    std::cout<<"v = "<<v<<"\n";
    std::cout<<"v.min() = "<<v.min()<<"\n";
    std::cout<<"v.max() = "<<v.max()<<"\n";
    std::cout<<"v.sum() = "<<v.sum()<<"\n";
    std::cout<<"v.product() = "<<v.product()<<"\n";
    std::cout<<"v.normsq() = "<<v.normsq()<<"\n\n";


    //all operations work for an arbitrary number of dimensions
    Tensor<double,2,2,2,3> s;
    s.zero();
    for(size_t i=0; i<s.size(); i++)
    {
        s[i] = i;
    }
    std::cout<<"Tensor<double,2,2,2,3> s = "<<s<<"\n";

    Tensor<size_t,4> index{0,1,1,2};
    std::cout<<"Tensor<size_t,4> index = "<<index<<"\n";
    std::cout<<"s[index] = "<<s[index]<<"\n";

    return 0;
}
