#include "Tensor.h"
#include <iostream>

namespace unittest
{
    namespace common
    {
        constexpr bool initialization()
        {
            int array[9] = {0,1,2, 3,4,5, 6,7,8};
            Tensor<int,3,3> tensor0{0,1,2, 3,4,5, 6,7,8};
            Tensor<int,3,3> tensor1 = {0,1,2, 3,4,5, 6,7,8};

            for(size_t i=0; i<9; i++)
            {
                if(array[i] != *((int*)&tensor0 + i))
                {
                    return false;
                }
                if(array[i] != *((int*)&tensor1 + i))
                {
                    return false;
                }
            }

            Tensor<int> tensor2{9};
            if(*((int*)&tensor2) != 9)
            {
                return false;
            }

            return true;
        }

        constexpr bool access()
        {
            Tensor<int,3,3,3> tensor{0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26};
            for(size_t i=0; i<27; i++)
            {
                if(tensor[i] != i)
                {
                    return false;
                }
            }

            for(size_t i=0; i<3; i++)
            {
                for(size_t j=0; j<3; j++)
                {
                    for(size_t k=0; k<3; k++)
                    {
                        if(tensor[{i,j,k}] != i*9 + j*3 + k)
                        {
                            return false;
                        }
                    }
                }
            }

            if((void*)&tensor != (void*)tensor.ptr())
            {
                return false;
            }

            return true;
        }

        constexpr bool dimensions()
        {
            Tensor<int,2,3,4> tensor;
            if(tensor.size() != 2*3*4)
            {
                return false;
            }
            if(tensor.ndim() != 3)
            {
                return false;
            }
            if(tensor.dim(0)!=2 || tensor.dim(1)!=3 || tensor.dim(2)!=4)
            {
                return false;
            }
            if(tensor.stride(0)!=12 || tensor.stride(1)!=4 || tensor.stride(2)!=1)
            {
                return false;
            }
            
            Tensor<int> scalar;
            if(scalar.size() != 1 || scalar.ndim() != 0)
            {
                return false;
            }
            return true;
        }

        constexpr bool zero()
        {
            Tensor<int,3> tensor{0,1,2};
            tensor.zero();
            for(size_t i=0; i<tensor.size(); i++)
            {
                if(tensor[i] != 0)
                {
                    return false;
                }
            }
            return true;
        }

        constexpr bool iterate()
        {
            Tensor<Tensor<size_t,3>, 8> expected = {Tensor<size_t,3>{1,1,1}, Tensor<size_t,3>{1,1,2}, Tensor<size_t,3>{1,2,1}, Tensor<size_t,3>{1,2,2}, Tensor<size_t,3>{2,1,1}, Tensor<size_t,3>{2,1,2}, Tensor<size_t,3>{2,2,1}, Tensor<size_t,3>{2,2,2}};

            Tensor<size_t,3> min{1,1,1};
            Tensor<size_t,3> max{2,2,2};
            Tensor<size_t,3> current = min;
            size_t i=0;
            for(bool looping=true; looping; looping=iterate(min, max, current))
            {
                if(current != expected[i])
                {
                    return false;
                }
                i++;
            }
            return true;
        }
        


    }



    namespace looped
    {
        constexpr bool geometrics()
        {
            Tensor<int,16> tensor;
            for(size_t i=0; i<tensor.size(); i++)
            {
                tensor[i] = i;
            }

            int min = 0;
            int max = tensor.size()-1;
            int sum = 0;
            int product = 1;
            int normsq = 0;
            for(size_t i=0; i<tensor.size(); i++)
            {
                sum += i;
                product *= i;
                normsq += i*i;
            }

            if(tensor.min()!=min || tensor.max()!=max || tensor.sum()!=sum || tensor.product()!=product || tensor.normsq()!=normsq)
            {
                return false;
            }
            return true;
        }

        constexpr bool comparison()
        {
            Tensor<int,16> a,b,c;
            for(size_t i=0; i<a.size(); i++)
            {
                a[i] = i;
                b[i] = i;
                c[i] = i;
            }
            c[3] = 12;

            if(a==b && b==a && a!=c && c!=a && b!=c && c!=b)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        constexpr bool algebra()
        {
            Tensor<int,16> a,b;
            for(size_t i=0; i<a.size(); i++)
            {
                a[i] = i;
                b[i] = i+1;
            }

            Tensor<int,16> c = a+b;
            Tensor<int,16> d = a-b;
            Tensor<int,16> e = a*b;
            Tensor<int,16> f = a/b;
            Tensor<int,16> g = +a;
            Tensor<int,16> h = -a;
            
            for(size_t i=0; i<a.size(); i++)
            {
                if(c[i] != i + (i+1))
                {
                    return false;
                }
                if(d[i] != i - (i+1))
                {
                    return false;
                }
                if(e[i] != i * (i+1))
                {
                    return false;
                }
                if(f[i] != i / (i+1))
                {
                    return false;
                }
                if(g[i] != i)
                {
                    return false;
                }
                if(h[i] != -1*i)
                {
                    return false;
                }
            }
            return true;
        }

        constexpr bool algebra_inplace()
        {
            Tensor<int,16> a,b;
            for(size_t i=0; i<a.size(); i++)
            {
                a[i] = i;
                b[i] = i+1;
            }

            Tensor<int,16> c = a;
            Tensor<int,16> d = a;
            Tensor<int,16> e = a;
            Tensor<int,16> f = a;
            c += b;
            d -= b;
            e *= b;
            f /= b;
            
            for(size_t i=0; i<a.size(); i++)
            {
                if(c[i] != i + (i+1))
                {
                    return false;
                }
                if(d[i] != i - (i+1))
                {
                    return false;
                }
                if(e[i] != i * (i+1))
                {
                    return false;
                }
                if(f[i] != i / (i+1))
                {
                    return false;
                }
            }
            return true;
        }

        constexpr bool vector_matrix_algebra()
        {
            Tensor<int,3> a{1,2,3};
            Tensor<int,3> b{4,5,6};
            Tensor<int,2,3> c{1,0,1,0,1,0};
            Tensor<int,3,3> d{1,0,0,0,1,0,0,0,1};

            if(cross(a,b) != Tensor<int,3>{-3,6,-3})
            {
                return false;
            }
            if(mvp(c,a) != Tensor<int,2>{4,2})
            {
                return false;
            }
            if(vmp(b,d) != Tensor<int,3>{4,5,6})
            {
                return false;
            }
            if(mmp(c,d) != Tensor<int,2,3>{1,0,1,0,1,0})
            {
                return false;
            }
            return true;
        }
    }


    void run()
    {
        std::cout<<"Unit Test\n=========\n\n";
        std::cout<<"Initialization:\t\t\t"          <<common::initialization()<<"\n";
        std::cout<<"Element Access:\t\t\t"          <<common::access()<<"\n";
        std::cout<<"Dimensions:\t\t\t"              <<common::dimensions()<<"\n";
        std::cout<<"Zeroing:\t\t\t"                 <<common::zero()<<"\n";
        std::cout<<"N-Dimensional Iterating:\t"     <<common::iterate()<<"\n";
        std::cout<<"Looped Geometrics:\t\t"         <<looped::geometrics()<<"\n";
        std::cout<<"Looped Comparison:\t\t"         <<looped::comparison()<<"\n";
        std::cout<<"Looped Algebra:\t\t\t"            <<looped::algebra()<<"\n";
        std::cout<<"Looped Algebra Inplace:\t\t"      <<looped::algebra_inplace()<<"\n";
        std::cout<<"Vector Matrix Algebra:\t\t"      <<looped::vector_matrix_algebra()<<"\n";
    }
}

int main()
{
    unittest::run();


    return 0;
}
