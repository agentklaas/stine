Stine
===

A single-file header-only tensor library.
It is designed to provide multidimensional arrays of fixed size on the stack.
Many computations, e.g. for element-access, are performed at compile time to deliver good performance.

Additionally, for small tensors, loops in most functions are replaced by recursive versions, to enhance performance even further.
With modern compiler optimizations and automatic loop unrolling this might not make much of a difference, though. 
Note, that you need a compiler that is at least capable of C++17.

To use in you own project, you simply have to include the header file (Tensor.h).
You can either copy the file, or you can add this project as a git submodule to your own to get the latest updates.
(I don't make any guarantees with regards to backwards compatability though. In fact, I expect the interface to change in the future.)


Files
===

Tensor.h
---
This file contains the Tensor struct and everything you would need in your own project.
The Tensor struct primarily serves as a container type with convenient accessors, but it does define some of the most important algebraic functions as well:

- operator+
- operator-
- operator*
- operator/
- operator==
- operator!=
- operator+=
- operator-=
- operator*=
- operator/=
- dot
- outer_product
- min 
- max
- sum
- product
- normsq



example.cpp
---
Refer to this file for a basic usage overview.
Having a look at this file is the easiest way to learn to use the Tensor struct.


unittest.cpp
---
Self-explanatory.
This is used in development to ensure nothing breaks when introducing changes.


CMakeLists.txt
---
Used to build the example and unittest programs.
It is kept very minimalistic, so you can instead just use your compiler of choice directly.

I recommend building in the "build" subdirectory.
Git is set to ignore this directory.
For example:
    
    mkdir build
    cd build
    cmake ..
    make
